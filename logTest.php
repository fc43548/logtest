<?

$str = "";
$int = 20;
        
for ($i = 1; $i <= $int ; $i++) {

    if ( $i%3 == 0 && $i%5 == 0 ) {

        if( in_array(3,  str_split($i)) || in_array(5,  str_split($i))){
            $str .= '"lucky", ';
        }
        else{
            $str .= '"fizzbuzz" ,';
        }
    }
    else if ( $i%3 == 0 || in_array(3,  str_split($i)) ) {

        if ($i%3 == 0 && in_array(3,  str_split($i))){
            $str .= '"fizz fizz", ';
            continue;
        }
        $str .= '"fizz", ';
    }
    else if ( $i%5 == 0 || in_array(5,  str_split($i))) {

        if ($i%5 == 0 && in_array(5,  str_split($i))){
            $str .= '"buzz buzz", ';
            continue;
        }
        $str .= '"buzz", ';
    }
    else {
        $str .= $i.", ";
    }
}

echo substr($str, 0, -2);

?>